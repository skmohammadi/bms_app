﻿import 'package:bmscience/models/User.dart';
import 'package:bmscience/models/chapter.dart';
import 'package:bmscience/models/comment.dart';
import 'package:bmscience/models/course.dart';
import 'package:bmscience/models/part.dart';
import 'package:bmscience/models/question.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/utils/courses_list.dart';
import 'package:bmscience/utils/exceptions.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'dart:convert';

class ApiService {
  AuthProvider authProvider;
  String token;

  // The AuthProvider is passed in when this class instantiated.
  // This provides access to the user token required for API calls.
  // It also allows us to log out a user when their token expires.
  ApiService([AuthProvider authProvider]) {
    print('api service');
    if (authProvider != null) {
      this.authProvider = authProvider;
      this.token = authProvider.token;
    }
  }

  final String api = 'http://bmscience.ir/api/v1';

  /*
  * Validates the response code from an API call.
  * A 401 indicates that the token has expired.
  * A 200 or 201 indicates the API call was successful.
  */
  Future<void> validateResponseStatus(int status, int validStatus) async {
    if (status == 401) {
      print('validateResponseStatus: 401');
      await authProvider.logOut(true);
      throw AuthException("401", "نشست کاربری شما منقضی شده است");
    }

    if (status != validStatus) {
//      throw new ApiException(status.toString(), "API Error");
    }
  }

  // Returns user.
  Future<User> getUser() async {
    final url = "$api/profile";

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token'
      },
    );

    await validateResponseStatus(response.statusCode, 200);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    dynamic data = apiResponse['data']['user'];

    User user = userFromJson(json.encode(data));

    return user;
  }

  // Returns a list of all courses.
  Future getCourses() async {
    final String url = "$api/courses";

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
      },
    );

    validateResponseStatus(response.statusCode, 200);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    List<dynamic> data = apiResponse['data']['courses'];

    List<Course> medicalCourses = coursesFromJson(json.encode(data[0]['lessons']));

    List<Course> dentistryCourses = coursesFromJson(json.encode(data[1]['lessons']));

    return CoursesList(medicalCourses, dentistryCourses);
  }

  Future getCourse(int id) async {
    final String url = "$api/lesson/$id";
    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
      },
    );

    Map<String, dynamic> apiResponse = json.decode(response.body);
    dynamic data = apiResponse['data']['lesson'];
    data['id'] = id; // TODO: Pass id value to API data

    Course course = courseFromJson(json.encode(data));
    return course;
  }

  Future getChapters(int courseId) async {
    final url = "$api/lesson/$courseId/chapters";

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    validateResponseStatus(response.statusCode, 200);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    List<Chapter> chapters = chaptersFromJson(json.encode(apiResponse['data']['chapters']));

    return chapters;
  }

  Future getParts(int chapterId) async {
    final url = "$api/chapter/$chapterId/parts";

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    validateResponseStatus(response.statusCode, 200);
    Map<String, dynamic> apiResponse = json.decode(response.body);
    List<Part> parts = partsFromJson(json.encode(apiResponse['data']['parts']));

    return parts;
  }

  Future purchasePart(int partId) async {
    String url = "$api/part/$partId/purchase";

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    await validateResponseStatus(response.statusCode, 204);

    bool result = false;
    if (response.statusCode == 204) {
      result = true;
    }
    print(result);

    return result;
  }

  Future getQuestions(List<int> selectedParts, int packageSize) async {
    final url = "$api/questions";

    Map body = {'package': packageSize.toString()};

    for (var i = 0; i < selectedParts.length; i++) {
      try {
        body['parts[$i]'] = selectedParts[i].toString();
      } catch (error) {
        print(error);
      }
    }

    final response = await http.post(
      url,
      body: body,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    validateResponseStatus(response.statusCode, 200);
    Map<String, dynamic> apiResponse = json.decode(response.body);
    List<Question> questions = questionsFromJson(json.encode(apiResponse['data']['questions']));

    return questions;
  }

  Future markAsFavoriteQuestion(int questionId) async {
    String url = "$api/favorite";

    Map body = {'question_id': questionId.toString()};

    print(body);

    final response = await http.post(
      url,
      body: body,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    await validateResponseStatus(response.statusCode, 204);
  }

  Future getComments(int courseId) async {
    String url = "$api/lesson/$courseId/comments/free";

    if (authProvider.status == Status.Authenticated) {
      url = "$api/lesson/$courseId/comments";
    }

    final response = await http.get(
      url,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    await validateResponseStatus(response.statusCode, 200);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    dynamic data = apiResponse['data'];

    List<Comment> comments;

    if (data.containsKey('yours')) {
      print('yours');

      comments = commentsFromJson(json.encode(data['others']['comments']));
      if (data['yours'] != null) {
        Comment currentUserComment = commentFromJson(
          json.encode(data['yours']),
          ownedByCurrentUser: true,
        );

        comments.insert(0, currentUserComment);
      }
    } else {
      comments = commentsFromJson(json.encode(apiResponse['data']['comments']));
    }

    return comments;
  }

  Future submitComment({int courseId, String text, int rating}) async {
    final url = "$api/comment";
    Map body = {'lesson_id': courseId.toString(), 'content': text, 'stars': rating.toString()};

    final response = await http.post(
      url,
      body: body,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
        HttpHeaders.authorizationHeader: 'Bearer $token',
      },
    );

    print(response.statusCode);
    await validateResponseStatus(response.statusCode, 200);

    Map<String, dynamic> apiResponse = json.decode(response.body);
    dynamic data = apiResponse['data'];
    print(data);
  }

  // Toggles the status of a todo.
  toggleTodoStatus(int id, String status) async {
    final url = 'https://laravelreact.com/api/v1/todo/$id';

    Map<String, String> body = {
      'status': status,
    };

    final response = await http.patch(url,
        headers: {
          HttpHeaders.acceptHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },
        body: body);

    validateResponseStatus(response.statusCode, 200);
  }

  // Adds a new todo.
  addTodo(String text) async {
    Map<String, String> body = {
      'value': text,
    };

    final response = await http.post(api,
        headers: {
          HttpHeaders.acceptHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer $token',
        },
        body: body);

    validateResponseStatus(response.statusCode, 201);

    // Returns the id of the newly created item.
    Map<String, dynamic> apiResponse = json.decode(response.body);
    int id = apiResponse['id'];
    return id;
  }
}
