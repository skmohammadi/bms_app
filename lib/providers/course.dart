﻿import 'package:bmscience/utils/courses_list.dart';
import 'package:flutter/material.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/models/course.dart';

enum CourseCategory { medical, dentistry }

class CourseProvider with ChangeNotifier {
  bool _initialized = false;

  // Stores separate lists for medical and dentistry courses.
  List<Course> _medicalCourses = List<Course>();
  List<Course> _dentistryCourses = List<Course>();

  // API Service
  ApiService apiService;

  // Provides access to private variables.
  bool get initialized => _initialized;
  List<Course> get medicalCourses => _medicalCourses;
  List<Course> get dentistryCourses => _dentistryCourses;

  // AuthProvider is required to instantiate our ApiService.
  // This gives the service access to the user token and provider methods.
  CourseProvider() {
    this.apiService = ApiService();
  }

  Future init() async {
    CoursesList coursesList = await apiService.getCourses();

    _initialized = true;
    _medicalCourses = coursesList.medical;
    _dentistryCourses = coursesList.dentistry;

    notifyListeners();
  }

  Future getCourseMeta(int index, int id, CourseCategory category) async {
    Course course = await apiService.getCourse(id);

    if (category == CourseCategory.medical) {
      _medicalCourses[index] = course;
    } else {
      _dentistryCourses[index] = course;
    }

    notifyListeners();
  }
}
