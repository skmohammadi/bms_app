﻿import 'dart:io';

import 'package:bmscience/views/Login.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

enum Status {
  Uninitialized,
  Authenticated,
  VerificationCodeSent,
  Authenticating,
  Unauthenticated,
  Registered,
  Registering
}

class AuthProvider with ChangeNotifier {
  Status _status = Status.Uninitialized;
  String _token;
  String _notification;

  Status get status => _status;
  String get token => _token;
  String get notification => _notification;

  final String api = 'http://bmscience.ir/api/v1';

  AuthProvider() {
    initAuthProvider();
  }

  initAuthProvider() async {
    print('initAuthProvider');
    String token = await getToken();
    if (token != null) {
      _token = token;
      _status = Status.Authenticated;
    } else {
      _status = Status.Unauthenticated;
      _notification = null;
    }
    notifyListeners();
  }

  /// Send authentication security code to user mobile
  Future<bool> sendCode(String mobile) async {
    _status = Status.Authenticating;
    _notification = null;
    notifyListeners();

    final url = "$api/authentication";
    Map<String, String> body = {
      'phone': mobile,
    };

    final response = await http.post(
      url,
      body: body,
    );

    print('auth code: ${response.body}');

    if (response.statusCode == 200) {
      Map<String, dynamic> apiResponse = json.decode(response.body);
      final code = apiResponse['data']['code']['content'];
      _status = Status.VerificationCodeSent;
      _notification = 'پیامک حاوی کد امنیتی ورود ارسال گردید ($code)';
//      _token = apiResponse['access_token'];
//      await storeUserData(apiResponse);
      notifyListeners();
      return true;
    }

    if (response.statusCode == 404) {
      _status = Status.Unauthenticated;
      _notification = 'شماره موبایل وارد شده نامعتبر است';
      notifyListeners();
      return false;
    }

    _status = Status.Unauthenticated;
    _notification = 'اختلال در سرور: دوباره امتحان کنید';
    notifyListeners();
    return false;
  }

  Future<bool> login(String mobile, String code) async {
    print('login');
    _status = Status.Authenticating;
    _notification = null;
    notifyListeners();

    final url = "$api/login";

    Map<String, String> body = {'phone': mobile, 'code': code};

    final response = await http.post(
      url,
      body: body,
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> apiResponse = json.decode(response.body);
      _status = Status.Authenticated;
      _token = apiResponse['data']['access_token'];

      await storeUserData(apiResponse);
      notifyListeners();
      return true;
    }

    if (response.statusCode == 401) {
      _status = Status.Unauthenticated;
      _notification = ('کد امنیتی نامعتبر است');
      notifyListeners();
      return false;
    }

    _status = Status.Unauthenticated;
    _notification = 'اختلال در سرور: دوباره امتحان کنید';
    notifyListeners();
    return false;
  }

  Future<bool> register(
      String mobile, String email, String universityEntrance) async {
    print('register');
    _status = Status.Registering;
    _notification = null;
    notifyListeners();

    final url = "$api/register";

    Map<String, String> body = {
      'phone': mobile,
      'email': email,
      'university_entrance': universityEntrance,
    };

    final response = await http.post(
      url,
      body: body,
      headers: {
        HttpHeaders.acceptHeader: 'application/json',
      },
    );

    if (response.statusCode == 200 || response.statusCode == 204) {
      _notification = ('ثبت نام با موفقیت انجام شد. در حال انتقال ...');
      _status = Status.Registered;
      notifyListeners();
      return true;
    }

    Map apiResponse = json.decode(response.body);
    if (response.statusCode == 422) {
      if (apiResponse['errors'].containsKey('phone') &&
          apiResponse['errors'].containsKey('email')) {
        _notification = 'شماره همراه و پست الکترونیکی قبلا انتخاب شده است';
      } else if (apiResponse['errors'].containsKey('email')) {
        _notification = apiResponse['errors']['email'][0];
      } else if (apiResponse['errors'].containsKey('phone')) {
        _notification = apiResponse['errors']['phone'][0];
      } else if (apiResponse['errors'].containsKey('university_entrance')) {
        _notification = apiResponse['errors']['university_entrance'][0];
      }

      _status = Status.Unauthenticated;
      notifyListeners();

      return false;
    }

    _status = Status.Unauthenticated;
    _notification = 'اختلال در سرور: دوباره امتحان کنید';
    notifyListeners();
    return false;
  }

  Future<bool> passwordReset(String email) async {
    final url = "$api/forgot-password";

    Map<String, String> body = {
      'email': email,
    };

    final response = await http.post(
      url,
      body: body,
    );

    if (response.statusCode == 200) {
      _notification = ('Reset sent. Please check your inbox.');
      notifyListeners();
      return true;
    }

    return false;
  }

  storeUserData(apiResponse) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    await storage.setString('token', apiResponse['data']['access_token']);
    await storage.setString('name', apiResponse['data']['user']['name']);
  }

  Future<String> getToken() async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String token = storage.getString('token');
//    String token =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZmIwMGViYjE3NmU1MjJkN2RkNjZkYjM5MDY0ODg1YTUzODBmNGVmZmU5MGUyOTg5ZjU3NmM4NjliMDc3ZGJiMjFiNTZkYzYyNjEyODMwZDYiLCJpYXQiOjE1ODc5MzM5ODcsIm5iZiI6MTU4NzkzMzk4NywiZXhwIjoxNTkzMjA0Mzg3LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.gWg_G0TvNKxkQRRCsNP8SKwIJURwFc8QAQo-7de4BSWDP2AyWvYnKJHg_9PYLOBIF07KW4MWUcSRHBj4kN8ZbFW2I5V1CUDUz7oyE2HAlXM-oTodTUTTzAeAqbQEN_lI_yjh--oNediOEfsb3SHM8K7neod0DArmCAjdDq9wFv2umpeirE7LShKcYdDZ6xCH_K2n6c77aY6ksRE3Yn9g_LeDDPoJm5bfwugMuTgTRhZB8TL2gY8Twxv3gmv2cLSSObumZC1UQWwGhZ2JKlmf45cmZYoLipQ3CGzqzBv9Qu0aMYQL_j3CUHMgr7m8CgEOVnfsoHzMT2O64PQszdg_d7CR1yHwGLkodyjvBc08R1CKiZBkAQEt8M8DoJJh2Qu6VXTwSvjLZamNMk6RjBXzngfWasnYsKrwNXtRm9BlU09lpXPSR4hSmPAINd5Ww0r5sgt6BzJwJRqptSMeHBUZTvugWXusnok4oKXTvEADnjekkCm9U1U5yCGE66CdOaRbrgCVsjy4_Y2vHcSCtVIcAX1ITWyPrSLgmT1zplaoGT0Dqf6KpMdyTuxLxoyUzCUtkSjLCXmkhWVSW1GbVGjwhL06A8JF45iIoVzPab-6lrUjLQZzfq1PatSneLcB6wRL8DbDUO4Enq8sqkd9jc4YRE0NBBX6Sk1-l0cPLprISSQ';
    return token;
  }

  logOut([bool tokenExpired = false]) async {
    _status = Status.Unauthenticated;

    if (tokenExpired == true) {
      _notification = ('نشست کاربری شما منقضی شده است. در حال انتقال ...');
    }

    final url = "$api/logout";
    await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
    );

    notifyListeners();

    SharedPreferences storage = await SharedPreferences.getInstance();
    await storage.clear();
  }
}

Future showLoginNeededDialog(BuildContext context) async {
  final String title = 'عدم دسترسی';
  final String message =
      'جهت دسترسی به این بخش، می‌بایست وارد حساب کاربری خود شوید.';

  AlertDialog alert = AlertDialog(
    title: Center(
      child: Text(title),
    ),
    content: Text(message),
    actions: <Widget>[
      FlatButton(
        child: Text('انصراف'),
        onPressed: Navigator.of(context).pop,
      ),
      FlatButton(
        child: Text('ورود'),
        onPressed: () async {
          Navigator.of(context).pop();
          await Navigator.pushNamed(
            context,
            LoginScreen.routeName,
          );
        },
      )
    ],
  );

  await showDialog(
    context: context,
    builder: (_) => alert,
  );
}
