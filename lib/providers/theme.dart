﻿import 'package:bmscience/theme/themes.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider with ChangeNotifier {
  bool isLightTheme = true;
  final themePreferenceKey = "is_light_theme";

  ThemeProvider() {
    init();
  }

  void init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.isLightTheme = prefs.getBool(themePreferenceKey) ?? true;

    notifyListeners();
  }

  ThemeData get getThemeData {
    return isLightTheme
        ? appThemeData[AppTheme.Light]
        : appThemeData[AppTheme.Dark];
  }

  set setThemeData(bool val) {
    if (val) {
      isLightTheme = true;
    } else {
      isLightTheme = false;
    }
    notifyListeners();

    // Save selected theme into SharedPreferences
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(themePreferenceKey, val);
    });
  }
}
