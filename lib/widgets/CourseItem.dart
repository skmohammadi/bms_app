﻿import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/views/Course.dart';
import 'package:bmscience/widgets/CourseRating.dart';
import 'package:bmscience/widgets/ListTileLeadingImage.dart';
import 'package:flutter/material.dart';

class CourseItem extends StatelessWidget {
  final index; // Position of course object in provider
  final id;
  final title;
  final rating;
  final icon;
  final category;

  CourseItem(
      {Key key,
      this.index,
      this.id,
      this.title,
      this.rating,
      this.icon,
      this.category})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white.withOpacity(.99),
      elevation: 1,
      child: ListTile(
        onTap: () {
          Navigator.pushNamed(
            context,
            CourseScreen.routeName,
            arguments: CourseScreenArguments(index, id, category),
          );
        },
        title: Text(title),
        subtitle: CourseRating(rating: rating),
        leading: ListTileLeadingImage(icon),
        trailing: const Icon(
          Icons.chevron_right,
          color: accentColor,
        ),
      ),
    );
  }
}
