﻿import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ListTileLeadingImage extends StatelessWidget {
  final String imageUrl;

  ListTileLeadingImage(this.imageUrl);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      width: 56,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: Theme.of(context).appBarTheme.color.withOpacity(.5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CachedNetworkImage(
          placeholder: (context, url) => Padding(
            padding: const EdgeInsets.all(20.0),
            child: CircularProgressIndicator(
              strokeWidth: 2,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
            ),
          ),
          imageUrl: imageUrl ?? '',
          errorWidget: (context, url, error) => const Image(
            image: AssetImage('assets/images/course.png'),
          ),
        ),
      ),
    );
  }
}
