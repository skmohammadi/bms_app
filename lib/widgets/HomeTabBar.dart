﻿import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/utils/app_icons.dart';
import 'package:flutter/material.dart';

class HomeTabBar extends StatefulWidget {
  const HomeTabBar(this._tabController, this.isLightTheme);

  final TabController _tabController;
  final bool isLightTheme;
  static int selectedTab;
  static Color unselectedBorderColor;

  @override
  _HomeTabBarState createState() => _HomeTabBarState();
}

class _HomeTabBarState extends State<HomeTabBar> {
  static int selectedTab;
  static Color unselectedBorderColor;

  @override
  Widget build(BuildContext context) {
    selectedTab = widget._tabController.index;
    unselectedBorderColor = widget.isLightTheme ? primaryColorLight : Colors.white;

    widget._tabController.animation.addListener(() {
      // skip unwanted state changes
      if (widget._tabController.indexIsChanging && selectedTab != widget._tabController.index) {
        setState(() {
          selectedTab = widget._tabController.index ?? 0;
        });
      }
    });

    widget._tabController.addListener(() {
      // skip unwanted state changes
      if (widget._tabController.indexIsChanging && selectedTab != widget._tabController.index) {
        setState(() {
          selectedTab = widget._tabController.index ?? 0;
        });
      }
    });

    return TabBar(
      controller: widget._tabController,
      unselectedLabelColor: widget.isLightTheme ? Colors.white : Colors.white,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorColor: primaryColor,
      labelColor: primaryColor,
      indicator: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
        border: Border.all(color: Colors.white, width: 1),
      ),
      tabs: [
        Tab(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: selectedTab == 0 ? Colors.transparent : unselectedBorderColor,
                width: 1,
              ),
            ),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Icon(BMSIcons.heartbeat),
                  SizedBox(width: 10),
                  Center(child: Text("پزشکی")),
                ],
              ),
            ),
          ),
        ),
        Tab(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: selectedTab == 1 ? Colors.transparent : unselectedBorderColor,
                width: 1,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const Icon(BMSIcons.teeth),
                SizedBox(width: 10),
                Center(child: const Text("دندان‌پزشکی")),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
