﻿import 'package:bmscience/providers/theme.dart';
import 'package:flutter/material.dart';

List<Widget> getAppBarActions(BuildContext context, ThemeProvider themeChanger,
    [Color color]) {
  return <Widget>[
    // App Share Button
    IconButton(
      color: color ?? Theme.of(context).iconTheme.color,
      icon: Icon(Icons.share),
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("اشتراک گذاری"),
              content: Text("متن خالی"),
            );
          },
        );
      },
    ),

    // Dark Mode Toggle Button
    Switch(
      inactiveThumbImage: AssetImage('assets/images/sun-black.png'),
      activeThumbImage: AssetImage('assets/images/moon-white.png'),
      activeColor: Colors.black,
      activeTrackColor: Colors.grey,
      value: !themeChanger.isLightTheme,
      onChanged: (value) {
        themeChanger.setThemeData = !value;
      },
    ),
  ];
}
