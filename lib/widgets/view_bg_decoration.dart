﻿import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

BoxDecoration getScreenBackground(bool isLightTheme) {
//  String bgImagePath = "assets/images/bg-${isLightTheme ? 'day' : 'night'}.jpg";
  String bgImagePath =
      "assets/images/${isLightTheme ? 'light' : 'night'}-bg.png";
  return BoxDecoration(
    image: DecorationImage(
        image: AssetImage(bgImagePath),
//        fit: BoxFit.contain,
        repeat: ImageRepeat.repeat),
  );
}
