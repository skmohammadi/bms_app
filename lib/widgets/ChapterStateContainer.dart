﻿import 'package:flutter/material.dart';
import 'package:bmscience/views/Chapter.dart';

class ChapterProvider extends StatefulWidget {
  final Widget child;
  final GlobalKey<ChapterFABState> fabKey;

  const ChapterProvider({Key key, @required this.child, this.fabKey}) : super(key: key);

  static _ChapterProviderState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedStateContainer>().data;
  }

  @override
  _ChapterProviderState createState() => _ChapterProviderState();
}

class _ChapterProviderState extends State<ChapterProvider> {
  List<int> selectedItems = [];

  void toggleSelection(int partId) {
    setState(() {
      if (selectedItems.contains(partId)) {
        selectedItems.remove(partId);
      } else {
        selectedItems.add(partId);
      }
    });

    if (selectedItems.isNotEmpty) {
      widget.fabKey.currentState.controller.forward();
    } else {
      widget.fabKey.currentState.controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedStateContainer(
      child: widget.child,
      data: this,
    );
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final _ChapterProviderState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) => true;
}
