﻿import 'package:bmscience/theme/themes.dart';
import 'package:flutter/material.dart';
import 'package:bmscience/widgets/Stars.dart';

class CourseRating extends StatelessWidget {
  final double rating;
  final bool singleStar;
  final Color color;
  const CourseRating({Key key, this.rating, this.singleStar, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StarDisplayWidget(
      color: color ?? secondaryTextColor,
      value: rating,
      singleStar: singleStar ?? false,
    );
  }
}
