﻿import 'package:bmscience/theme/themes.dart';
import 'package:flutter/material.dart';

class StarDisplayWidget extends StatelessWidget {
  final double value;
  final bool singleStar;
  final Widget filledStar;
  final Widget unfilledStar;
  final double size;
  final Color color;
  final int marginFactor;

  const StarDisplayWidget({
    Key key,
    this.value = 0,
    this.singleStar = false,
    this.filledStar,
    this.unfilledStar,
    this.color = Colors.orange,
    this.size = 20,
    this.marginFactor = 5, // TODO: remove if is redundant
  })  : assert(value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var numOfStars = singleStar ? 1 : 5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: List.generate(numOfStars, (index) {
        return Icon(
          index < value ? filledStar ?? Icons.star : unfilledStar ?? Icons.star_border,
          color: color,
          size: size,
        );
      }),
    );
  }
}

class StarRating extends StatelessWidget {
  final void Function(int index) onChanged;
  final int value;
  final IconData filledStar;
  final IconData unfilledStar;
  final double size;
  final Color color;
  final int marginFactor;

  const StarRating({
    Key key,
    @required this.onChanged,
    this.value = 0,
    this.filledStar,
    this.unfilledStar,
    this.color,
    this.size = 20,
    this.marginFactor = 5,
  })  : assert(value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        return RawMaterialButton(
          child: Icon(
            index < value ? filledStar ?? Icons.star : unfilledStar ?? Icons.star_border,
            color: color ?? secondaryTextColor,
            size: size,
          ),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          shape: CircleBorder(),
          constraints: BoxConstraints.expand(width: size, height: size),
          padding: EdgeInsets.zero,
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onPressed: onChanged != null
              ? () {
                  onChanged(value == index + 1 ? index : index + 1);
                }
              : null,
        );
      }),
    );
  }
}
