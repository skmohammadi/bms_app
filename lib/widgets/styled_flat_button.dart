﻿import 'package:bmscience/theme/styles.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/utils/HexColor.dart';
import 'package:flutter/material.dart';

class StyledFlatButton extends StatelessWidget {
  final String text;
  final onPressed;
  final Color color;
  final double radius;
  final bool isLoading;
  final bool disabled;

  const StyledFlatButton(
    this.text, {
    this.onPressed,
    Key key,
    this.color,
    this.radius,
    this.isLoading = false,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _color = color ?? primaryColor;
    if (disabled) _color = lighten(_color, .1);

    return FlatButton(
      color: _color,
      splashColor: _color,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18.0),
        child: (isLoading
            ? SizedBox(
                height: 16,
                width: 16,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
                ),
              )
            : Text(
                this.text,
                style: Styles.p.copyWith(
                  color: Colors.white,
                  height: 1,
                  fontWeight: FontWeight.w500,
                ),
              )),
      ),
      onPressed: (disabled || isLoading) ? null : this.onPressed,
      onLongPress: null,
      disabledColor: _color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius ?? 4.0),
        side: BorderSide(
          color: _color,
          width: 2,
        ),
      ),
    );
  }
}
