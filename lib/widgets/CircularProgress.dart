﻿import 'dart:math';

import 'package:flutter/material.dart';

class CircularProgress extends CustomPainter {
  final double currentProgress;
  double strokeWidth = 5;
  Color backgroundColor = Colors.white;
  Color foregroundColor = Colors.black;

  CircularProgress(this.currentProgress,
      {this.strokeWidth, this.backgroundColor, this.foregroundColor});

  @override
  void paint(Canvas canvas, Size size) {
    Paint outerCircle = Paint()
      ..strokeWidth = strokeWidth
      ..color = backgroundColor
      ..style = PaintingStyle.stroke;

    Paint fillingArc = Paint()
      ..strokeWidth = strokeWidth
      ..color = foregroundColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.butt;

    Offset center = Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2) - strokeWidth;

    canvas.drawCircle(center, radius, outerCircle);

    double angel = 2 * pi * (currentProgress / 100);

    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), -pi / 2,
        angel, false, fillingArc);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
