﻿import 'package:bmscience/models/course.dart';
import 'package:bmscience/providers/course.dart';
import 'package:flutter/material.dart';
import 'CourseItem.dart';

/* TODO: Prevent redundant build */

class CoursesList extends StatelessWidget {
  final List<Course> courses;
  final CourseCategory category;

  CoursesList(this.courses, this.category);

  @override
  Widget build(BuildContext context) {
    print('courses: $category');
    return ListView.builder(
      itemCount: courses.length,
      itemBuilder: (BuildContext ctx, int index) {
        final course = courses[index];

        return CourseItem(
          key: Key((course.id).toString()),
          index: index,
          title: course.title,
          rating: course.stars,
          icon: course.icon,
          category: category,
          id: course.id,
        );
      },
    );
  }
}
