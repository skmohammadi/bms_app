﻿import 'package:bmscience/theme/themes.dart';
import 'package:flutter/material.dart';

class CourseTabBar extends StatefulWidget {
  const CourseTabBar(this._tabController, this.isLightTheme);

  final TabController _tabController;
  final bool isLightTheme;
  static int selectedTab;
  static Color unselectedBorderColor;

  @override
  _CourseTabBarState createState() => _CourseTabBarState();
}

class _CourseTabBarState extends State<CourseTabBar> {
  static int selectedTab;
  static Color unselectedBorderColor;

  @override
  Widget build(BuildContext context) {
    selectedTab = widget._tabController.index;
    unselectedBorderColor = widget.isLightTheme ? primaryColorLight : Colors.white;

    widget._tabController.animation.addListener(() {
      // skip unwanted state changes
      if (widget._tabController.indexIsChanging && selectedTab != widget._tabController.index) {
        setState(() {
          selectedTab = widget._tabController.index ?? 0;
        });
      }
    });

    widget._tabController.addListener(() {
      // skip unwanted state changes
      if (widget._tabController.indexIsChanging && selectedTab != widget._tabController.index) {
        setState(() {
          selectedTab = widget._tabController.index ?? 0;
        });
      }
    });

    return TabBar(
      controller: widget._tabController,
      unselectedLabelColor: widget.isLightTheme ? primaryColor : Colors.white,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorColor: primaryColor,
      labelColor: Colors.white,
      indicator: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: primaryColor,
        border: Border.all(color: primaryColor, width: 1),
      ),
      tabs: [
        Tab(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: selectedTab == 0 ? Colors.transparent : unselectedBorderColor,
                width: 1,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(child: Text("توضیحات")),
              ],
            ),
          ),
        ),
        Tab(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(
                color: selectedTab == 1 ? Colors.transparent : unselectedBorderColor,
                width: 1,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(child: const Text("نظرات")),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
