﻿import 'question.dart';

class Quiz {
  int _questionNumber = 0;
  List<Question> _questionBank;
  Quiz(this._questionBank);

  void reset() {
    _questionNumber = 0;
  }

  bool isFinished() {
//    return _questionNumber >= (2) ? true : false;
    return _questionNumber >= (_questionBank.length - 1) ? true : false;
  }

  void nextQuestion() {
    if (_questionNumber < _questionBank.length - 1) {
      _questionNumber++;
    }
  }

  int get getQuestionsCount => _questionBank.length;
  int get currentQuestionIndex => _questionNumber;

  int get currentQuestionId => _questionBank[_questionNumber].id;

  String getQuestionText() {
    return _questionBank[_questionNumber].questionContent;
//    return _questionBank[index].questionContent;
  }

  bool isQuestionBookmarked() {
    return _questionBank[_questionNumber].bookmarked;
  }

  void setQuestionBookmarked() {
    _questionBank[_questionNumber].setBookmarked();
  }

  String getQuestionImage() {
    return _questionBank[_questionNumber].questionImage;
  }

  List<String> getQuestionOptions() {
    return [
      _questionBank[_questionNumber].firstOption,
      _questionBank[_questionNumber].secondOption,
      _questionBank[_questionNumber].thirdOption,
      _questionBank[_questionNumber].fourthOption,
    ];
  }

  int getCorrectAnswer() {
    return _questionBank[_questionNumber].answer;
  }
}
