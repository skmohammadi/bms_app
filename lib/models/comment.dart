﻿import 'dart:convert';

List<Comment> commentsFromJson(String str) {
  return List<Comment>.from(json.decode(str).map((x) {
    return Comment.fromJson(x);
  }));
}

Comment commentFromJson(String str, {bool ownedByCurrentUser}) {
  return Comment.fromJson(json.decode(str), owned: ownedByCurrentUser);
}

class Comment {
  String user;
  String content;
  String stars;
  String createdAt;

  bool ownedByCurrentUser;

  Comment({this.user, this.content, this.stars, this.createdAt});

  Comment.fromJson(Map<String, dynamic> json, {bool owned}) {
    ownedByCurrentUser = owned != null;
    user = json['user'];
    content = json['content'];
    stars = json['stars'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ownedByCurrentUser'] = this.ownedByCurrentUser;
    data['user'] = this.user;
    data['content'] = this.content;
    data['stars'] = this.stars;
    data['created_at'] = this.createdAt;
    return data;
  }
}
