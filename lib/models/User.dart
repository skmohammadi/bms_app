﻿import 'dart:convert';

User userFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromJson(jsonData);
}

String userToJson(List<User> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  String name;
  String email;
  String melliCode;
  int universityEntrance;

  User({this.name, this.email, this.melliCode, this.universityEntrance});

  factory User.fromJson(Map<String, dynamic> json) => new User(
        name: json["name"],
        email: json["email"],
        melliCode: json["melliCode"],
        universityEntrance: json["university_entrance"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "mellicode": melliCode,
        "university_entrance": universityEntrance,
      };
}
