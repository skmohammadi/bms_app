﻿import 'dart:convert';

List<Chapter> chaptersFromJson(String str) {
  return List<Chapter>.from(json.decode(str).map((x) {
    return Chapter.fromJson(x);
  }));
}

class Chapter {
  int id;
  String title;
  int price;
  int downloadsCount;
  int questionsCount;
  int partsCount;
  String icon;
  int favoritesCount;

  Chapter({
    this.id,
    this.title,
    this.price,
    this.downloadsCount,
    this.questionsCount,
    this.partsCount,
    this.icon,
    this.favoritesCount,
  });

  Chapter.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    price = json['price'];
    downloadsCount = json['downloads_count'];
    questionsCount = json['questions_count'];
    partsCount = json['parts_count'];
    icon = json['icon'];
    favoritesCount = json['favorites_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['price'] = this.price;
    data['downloads_count'] = this.downloadsCount;
    data['questions_count'] = this.questionsCount;
    data['parts_count'] = this.partsCount;
    data['icon'] = this.icon;
    data['favorites_count'] = this.favoritesCount;
    return data;
  }
}
