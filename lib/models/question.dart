﻿import 'dart:convert';

List<Question> questionsFromJson(String str) {
  return List<Question>.from(json.decode(str).map((x) {
    return Question.fromJson(x);
  }));
}

class Question {
  int id;
  String questionImage;
  String questionContent;
  String firstOption;
  String secondOption;
  String thirdOption;
  String fourthOption;
  int answer;
  String answerContent;
  String answerImage;
  bool bookmarked;

  Question(
      {this.id,
      this.questionImage,
      this.questionContent,
      this.firstOption,
      this.secondOption,
      this.thirdOption,
      this.fourthOption,
      this.answer,
      this.answerContent,
      this.answerImage,
      this.bookmarked});

  Question.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    questionImage = json['question_image'];
    questionContent = json['question_content'];
    firstOption = json['first_option'];
    secondOption = json['second_option'];
    thirdOption = json['third_option'];
    fourthOption = json['fourth_option'];
    answer = json['answer'];
    answerContent = json['answer_content'];
    answerImage = json['answer_image'];
    bookmarked = json['favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question_image'] = this.questionImage;
    data['question_content'] = this.questionContent;
    data['first_option'] = this.firstOption;
    data['second_option'] = this.secondOption;
    data['third_option'] = this.thirdOption;
    data['fourth_option'] = this.fourthOption;
    data['answer'] = this.answer;
    data['answer_content'] = this.answerContent;
    data['answer_image'] = this.answerImage;
    data['bookmarked'] = this.bookmarked;
    return data;
  }

  void setBookmarked() => bookmarked = true;
}
