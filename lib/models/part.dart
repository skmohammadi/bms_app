﻿import 'dart:convert';

List<Part> partsFromJson(String str) {
  return List<Part>.from(json.decode(str).map((x) {
    return Part.fromJson(x);
  }));
}

class Part {
  int id;
  String title;
  String price;
  int downloadsCount;
  int questionsCount;

  Part(
      {this.id,
      this.title,
      this.price,
      this.downloadsCount,
      this.questionsCount});

  Part.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    price = json['price'].toString();
    downloadsCount = json['downloads_count'];
    questionsCount = json['questions_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['price'] = this.price;
    data['downloads_count'] = this.downloadsCount;
    data['questions_count'] = this.questionsCount;
    data['purchased'] = this.isPurchased;
    return data;
  }

  void setPurchased() {
    price = 'purchased';
  }

  bool isPurchased() {
    return price == 'purchased';
  }
}
