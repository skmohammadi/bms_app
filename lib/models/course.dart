﻿import 'dart:convert';

List<Course> coursesFromJson(String str) {
  return List<Course>.from(json.decode(str).map((x) {
    return Course.fromJson(x);
  }));
}

Course courseFromJson(String str) {
  return Course.fromJson(json.decode(str));
}

String coursesToJson(List<Course> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

String courseToJson(Course course) => json.encode(course.toJson());

class Course {
  int id;
  String title;
  double stars;
  String icon;
  String description;
  int downloadsCount;
  int questionsCount;
  int chaptersCount;
  List<String> images;
  String updatedAt;
  bool initialized = false;

  Course({
    this.id,
    this.title,
    this.stars,
    this.icon,
    this.description,
    this.downloadsCount,
    this.questionsCount,
    this.chaptersCount,
    this.images,
    this.updatedAt,
    this.initialized,
  });

  Course.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    stars = json['stars'].toDouble();
    icon = json['icon'];
    description = json['description'] ?? null;
    downloadsCount = json['downloads_count'] ?? null;
    questionsCount = json['questions_count'] ?? null;
    chaptersCount = json['chapters_count'] ?? null;
    images = json['images'] != null ? json['images'].cast<String>() : null;
    updatedAt = json['updated_at'];
    initialized = json['description'] != null ? true : false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['id'] = this.id;
    data['title'] = this.title;
    data['stars'] = this.stars;
    data['icon'] = this.icon;
    data['description'] = this.description;
    data['downloads_count'] = this.downloadsCount;
    data['questions_count'] = this.questionsCount;
    data['chapters_count'] = this.chaptersCount;
    data['images'] = this.images;
    data['updated_at'] = this.updatedAt;
    data['initialized'] = this.initialized;

    return data;
  }
}
