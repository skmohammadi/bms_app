import 'package:bmscience/providers/course.dart';
import 'package:bmscience/routes.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/providers/auth.dart';

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => ThemeProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => AuthProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => CourseProvider(),
          ),
        ],
        child: App(),
      ),
    );

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer3<ThemeProvider, AuthProvider, CourseProvider>(
        builder: (context, theme, user, course, child) {
      return MaterialApp(
        title: 'BMS',
        debugShowCheckedModeBanner: false,
        theme: theme.getThemeData,
        builder: (context, child) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: child,
          );
        },
        initialRoute: '/',
        routes: routes,
//        home: HomeScreen(),
      );
    });
  }
}
