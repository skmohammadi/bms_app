﻿class Validate {
  // RegEx pattern for validating phone number.
  static Pattern mobilePattern = r"^(09)?[0-9]{9}$";
  static RegExp mobileRegEx = RegExp(mobilePattern);

  // RegEx pattern for validating verification code.
  static Pattern codePattern = r"[0-9]{5}$";
  static RegExp codeRegEx = RegExp(codePattern);

  // RegEx pattern for validating email addresses.
  static Pattern emailPattern =
      r"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$";
  static RegExp emailRegEx = RegExp(emailPattern);

  // RegEx pattern for validating university entrance year.
  static Pattern universityEntrancePattern = r"^(?=\d{4}$)(13)\d+$";
  static RegExp universityEntranceRegEx = RegExp(universityEntrancePattern);

  // Validates an phone number.
  static bool isMobile(String value) {
    if (mobileRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  // Validates an verification code.
  static bool isCode(String value) {
    if (codeRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  // Validates an email address.
  static bool isEmail(String value) {
    if (emailRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  // Validates an university entrance year.
  static bool isUniversityEntrance(String value) {
    if (universityEntranceRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  /// Returns an error message if mobile does not validate.
  static String validateMobile(String value) {
    String mobile = value.trim();
    if (mobile.isEmpty) {
      return 'شماره همراه را وارد کنید';
    }
    if (!isMobile(mobile)) {
      return 'شماره همراه معتبر نیست';
    }
    return null;
  }

  /// Returns an error message if verification code does not validate.
  static String validateCode(String value) {
    String code = value.trim();

    if (code.isEmpty) {
      return 'کد امنیتی را وارد کنید';
    }
    if (!isCode(code)) {
      return 'کد امنیتی معتبر نیست';
    }
    return null;
  }

  /// Returns an error message if email does not validate.
  static String validateEmail(String value) {
    String email = value.trim();
    if (email.isEmpty) {
      return 'فیلد پست الکترونیکی ضروری است';
    }
    if (!isEmail(email)) {
      return 'پست الکترونیکی را صحیح وارد کنید';
    }
    return null;
  }

  /// Returns an error message if email does not validate.
  static String validateUniversityEntrance(String value) {
    String universityEntrance = value.trim();
    if (universityEntrance.isEmpty) {
      return 'فیلد ورودی سال ضروری است';
    }
    if (!isUniversityEntrance(universityEntrance)) {
      return 'سال ورود صحیح نیست';
    }
    return null;
  }

  /*
   * Returns an error message if required field is empty.
   */
  static String requiredField(String value, String message) {
    if (value.trim().isEmpty) {
      return message;
    }
    return null;
  }
}
