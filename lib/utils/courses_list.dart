﻿import 'package:bmscience/models/course.dart';

class CoursesList {
  final List<Course> medical;
  final List<Course> dentistry;
  CoursesList(this.medical, this.dentistry);
}
