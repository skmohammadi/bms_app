﻿import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

void showNotification(
  BuildContext context, {
  String message,
  Function onDismiss,
  FlushbarPosition position,
}) {
  Flushbar(
    message: message,
    duration: Duration(seconds: 2),
    margin: EdgeInsets.all(8),
    borderRadius: 8,
    flushbarPosition: position ?? FlushbarPosition.BOTTOM,
    onStatusChanged: (status) {
      if (status == FlushbarStatus.DISMISSED && onDismiss != null) {
        onDismiss();
      }
    },
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
  )..show(context);
}
