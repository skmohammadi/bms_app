import 'package:flutter/material.dart';
//import 'package:flutter/widgets.dart';

class BMSIcons {
  BMSIcons();

  static const _kFontFam = 'BMS';
  static const _kFontPkg = null;

  static const IconData teeth =
      IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData heartbeat =
      IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData user_md =
      IconData(0xf0f0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
