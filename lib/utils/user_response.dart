﻿import 'package:bmscience/models/User.dart';

class UserResponse {
  final User user;
  final String apiMore;
  UserResponse(this.user, this.apiMore);
}
