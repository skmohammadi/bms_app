﻿import 'package:bmscience/views/Chapter.dart';
import 'package:bmscience/views/Chapters.dart';
import 'package:bmscience/views/Course.dart';
import 'package:bmscience/views/Home.dart';
import 'package:bmscience/views/Login.dart';
import 'package:bmscience/views/Profile.dart';
import 'package:bmscience/views/Quiz.dart';
import 'package:bmscience/views/Register.dart';
import 'package:flutter/widgets.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  HomeScreen.routeName: (BuildContext context) => HomeScreen(),
  CourseScreen.routeName: (BuildContext context) => CourseScreen(),
  ChaptersScreen.routeName: (BuildContext context) => ChaptersScreen(),
  ChapterScreen.routeName: (BuildContext context) => ChapterScreen(),
  QuizScreen.routeName: (BuildContext context) => QuizScreen(),
  LoginScreen.routeName: (BuildContext context) => LoginScreen(),
  RegisterScreen.routeName: (BuildContext context) => RegisterScreen(),
  ProfileScreen.routeName: (BuildContext context) => ProfileScreen(),
};
