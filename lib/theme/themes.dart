﻿import 'package:bmscience/utils/HexColor.dart';
import 'package:flutter/material.dart';

enum AppTheme { Light, Dark }

/// Returns enum value name without enum class name.
String enumName(AppTheme anyEnum) {
  return anyEnum.toString().split('.')[1];
}

/// Color Palette
//const Color primaryColor = Color(0xFFE91E63);
//const Color primaryColorDark = Color(0xFFC2185B);
//const Color primaryColorLight = Color(0xFFF8BBD0);
//const Color accentColor = Color(0xFF448AFF);
//const Color primaryTextColor = Color(0xFF212121);
//const Color secondaryTextColor = Color(0xFF757575);
//const Color dividerColor = Color(0xFFBDBDBD);
const Color primaryColor = Color(0xFF6f96ff);
const Color primaryColorDark = Color(0xFF00796B);
const Color primaryColorLight = Color(0xFF8DB4FF);
const Color accentColor = Color(0xFF448AFF);
const Color primaryTextColor = Color(0xFF212121);
const Color secondaryTextColor = Color(0xFF757575);
const Color dividerColor = Color(0xFFBDBDBD);

final appThemeData = {
  AppTheme.Light: ThemeData(
    brightness: Brightness.light,
    primaryColor: primaryColor,
    primaryColorLight: primaryColorLight,
    primarySwatch: Colors.deepPurple,
    primaryIconTheme: IconThemeData(
      color: Colors.white,
    ),
    primaryTextTheme: TextTheme(title: TextStyle(color: primaryTextColor)),
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(title: TextStyle(fontFamily: 'Shabnam FD', color: Colors.white)),
      color: primaryColor.withOpacity(.75),
      elevation: 2,
    ),
    accentColor: accentColor,
    tabBarTheme: TabBarTheme(unselectedLabelColor: primaryColor),
    accentIconTheme: IconThemeData(color: Colors.white),
    dividerColor: Colors.white54,
    fontFamily: 'Shabnam FD',
  ),
  AppTheme.Dark: ThemeData(
    brightness: Brightness.dark,
    primaryColor: primaryColor,
    primaryColorDark: darken(hexToColor('AA3C55'), 0.4),
    primarySwatch: Colors.grey,
    appBarTheme: AppBarTheme(
      color: Colors.black54,
      elevation: 0,
    ),
    tabBarTheme: TabBarTheme(unselectedLabelColor: Colors.white),
    backgroundColor: Color(0xFF000000),
    accentColor: HexColor('5EB3F3'),
    accentIconTheme: IconThemeData(color: Colors.black),
    dividerColor: Colors.black54,
    fontFamily: 'Shabnam FD',
  )
};
