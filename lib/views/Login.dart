﻿import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/theme/styles.dart';
import 'package:bmscience/utils/forms.dart';
import 'package:bmscience/utils/network_util.dart';
import 'package:bmscience/utils/validate.dart';
import 'package:bmscience/widgets/styled_flat_button.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/login';

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: WillPopScope(
        onWillPop: () async {
          if (authProvider.status == Status.Authenticating) {
            return false;
          }
          if (authProvider.status != Status.Unauthenticated) {
            authProvider.initAuthProvider();
          }

          return true;
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text('ورود'),
            leading: BackButton(
              onPressed: () {
                if (authProvider.status != Status.Unauthenticated ||
                    authProvider.notification != null) {
                  authProvider.initAuthProvider();
                }
                Navigator.of(context).pop();
              },
            ),
            actions: <Widget>[
              // Dark Mode Toggle Button
              Switch(
                inactiveThumbImage: AssetImage('assets/images/sun-black.png'),
                activeThumbImage: AssetImage('assets/images/moon-white.png'),
                activeColor: Colors.black,
                activeTrackColor: Colors.grey,
                value: !themeProvider.isLightTheme,
                onChanged: (value) {
                  themeProvider.setThemeData = !value;
                },
              ),
            ],
          ),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).appBarTheme.color,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Theme.of(context).appBarTheme.color,
                      width: 1,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: LogInForm(),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class LogInForm extends StatefulWidget {
  const LogInForm({Key key}) : super(key: key);

  @override
  LogInFormState createState() => LogInFormState();
}

class LogInFormState extends State<LogInForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String mobile;
  String code; // Verification Code

  static bool verificationCodeSent;

  @override
  void initState() {
    verificationCodeSent = false;
    super.initState();
  }

  Future<void> submit() async {
    final form = _formKey.currentState;
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    if (form.validate()) {
      if (!await NetworkUtil().hasInternet()) {
        showNotification(context,
            message: 'اتصال دستگاه به اینترنت را بررسی کنید');
      } else {
        if (verificationCodeSent) {
          await Provider.of<AuthProvider>(context, listen: false)
              .login(mobile, code);
        } else {
          await Provider.of<AuthProvider>(context, listen: false)
              .sendCode(mobile);
          if (authProvider.notification != null) {
            showNotification(
              context,
              message: authProvider.notification,
            );
          }
        }

        _formKey.currentState.reset();

        // Redirect to previous screen
        if (authProvider.status == Status.Authenticated) {
          // Pass true to back: user is logged-in
          Navigator.of(context).pop(true);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    if (authProvider.status == Status.VerificationCodeSent) {
      verificationCodeSent = true;
    }

    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          Consumer<AuthProvider>(
//            builder: (context, provider, child) =>
//                provider.status == Status.Authenticating
//                    ? Center(
//                        child: CircularProgressIndicator(),
//                      )
//                    : null,
//          ),
          TextFormField(
            enableSuggestions: false,
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            style: TextStyle(letterSpacing: 6),
            maxLength: (!verificationCodeSent ? 11 : 6),
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
            ],
            decoration: Styles.input.copyWith(
              errorText: null,
              counterText: "",
              labelText: (!verificationCodeSent ? 'شماره همراه' : 'کد امنیتی'),
              hintText: (!verificationCodeSent ? '09XXXXXXXXX' : 'XXXXXX'),
              icon: Icon(Icons.phone_android),
            ),
            validator: (value) {
              if (!verificationCodeSent) {
                mobile = value.trim();
                return Validate.validateMobile(value);
              }

              code = value.trim();
              return Validate.validateCode(value);
            },
          ),
          SizedBox(height: 45.0),
          StyledFlatButton(
            (!verificationCodeSent ? 'ورود به حساب' : 'تأیید کد امنیتی'),
            onPressed: () {
              FocusScope.of(context).unfocus(); // un-focus opened keyboard
              submit();
            },
            isLoading: authProvider.status == Status.Authenticating,
          ),
          SizedBox(height: 20.0),
          [Status.VerificationCodeSent, Status.Authenticating]
                  .contains(authProvider.status)
              ? null
              : StyledFlatButton(
                  'حساب کاربری ندارم',
                  onPressed: () {
                    Navigator.pushNamed(context, '/register');
                  },
                  color: Theme.of(context).accentColor,
                ),
        ]..removeWhere((widget) => widget == null),
      ),
    );
  }
}
