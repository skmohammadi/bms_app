﻿import 'package:bmscience/models/comment.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/course.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/theme/styles.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/utils/validate.dart';
import 'package:bmscience/views/Chapters.dart';
import 'package:bmscience/views/Login.dart';
import 'package:bmscience/widgets/CourseRating.dart';
import 'package:bmscience/widgets/CourseTabBar.dart';
import 'package:bmscience/widgets/Stars.dart';
import 'package:bmscience/widgets/getAppBarActions.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CourseContent extends StatefulWidget {
  final int index;
  final int courseId;
  final CourseCategory courseCategory;

  CourseContent(this.index, this.courseId, this.courseCategory);

  @override
  _CourseContentState createState() => _CourseContentState();
}

class _CourseContentState extends State<CourseContent> with SingleTickerProviderStateMixin {
  TabController _tabController;
  Future _getCourse;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
    _getCourse = getCourse();
  }

  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Future getCourse() async {
    print('getCourse ${widget.courseId}');
    final courseProvider = Provider.of<CourseProvider>(context, listen: false);

    // Check if the course has completely initialized
    if (!courseProvider.medicalCourses[widget.index].initialized) {
      try {
        await courseProvider.getCourseMeta(
          widget.index,
          widget.courseId,
          widget.courseCategory,
        );
      } catch (error) {
        throw Exception(error.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final courseProvider = Provider.of<CourseProvider>(context, listen: false);

    final course = courseProvider.medicalCourses[widget.index];

    return FutureBuilder(
      future: _getCourse,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text("${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          return SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                DefaultTextStyle(
                  child: Container(
                    color: primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white70,
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                child: SizedBox(
                                  width: 140.0,
                                  height: 200.0,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        CachedNetworkImage(
                                          placeholder: (context, url) => Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(Colors.grey),
                                            ),
                                          ),
                                          imageUrl: course.icon,
                                          errorWidget: (context, url, error) => const Image(
                                            image: AssetImage('assets/images/course.png'),
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: primaryColor,
                                              borderRadius: BorderRadius.circular(50)),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 4.0, horizontal: 10),
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                CourseRating(
                                                  color: Colors.white,
                                                  rating: course.stars,
                                                  singleStar: true,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text('${course.stars}')
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: SizedBox(
                                  height: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: <Widget>[
                                        Text(
                                          course.title,
                                          style: TextStyle(fontSize: 24),
                                        ),
                                        Text(
                                          'دانلود: ${course.downloadsCount}',
                                        ),
                                        Text(
                                          'تعداد سوال: ${course.questionsCount}',
                                        ),
                                        Text(
                                          'تعداد سوال: ${course.chaptersCount}',
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Text('بروزرسانی: '),
                                            Text(
                                              '${course.updatedAt}',
                                              textDirection: TextDirection.ltr,
                                            )
                                          ],
                                        ),
//                                        Spacer(),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        RaisedButton.icon(
                                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                          elevation: 2,
                                          icon: Icon(Icons.library_books),
                                          label: Text('مشاهده فصل‌ها'),
                                          color: Colors.white,
                                          textColor: primaryColor,
                                          onPressed: () {
                                            Navigator.pushNamed(
                                              context,
                                              ChaptersScreen.routeName,
                                              arguments: ChaptersScreenArguments(
                                                course.id,
                                                course.title,
                                              ),
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  style: TextStyle(fontFamily: 'Shabnam FD', color: Colors.white),
                ),
                SizedBox(
                  height: 25,
                ),

                /// Course Image Slider
                course.images.length > 0
                    ? Container(
                        height: 200,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: course.images.length,
                          itemBuilder: (context, index) {
                            final url = course.images[index];
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Container(
                                  color: Colors.white54,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Center(
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) => Container(
                                          width: 80,
                                          height: 80,
                                          child: SizedBox(
                                            height: 20,
                                            width: 20,
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(Colors.grey),
                                            ),
                                          ),
                                        ),
                                        imageUrl: url,
                                        errorWidget: (context, url, error) => const Image(
                                          image: AssetImage('assets/images/course.png'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Container(),

                SizedBox(
                  height: 25,
                ),

                /// Course Footer Tabs
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).appBarTheme.color,
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      CourseTabBar(_tabController,
                          Provider.of<ThemeProvider>(context, listen: false).isLightTheme),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                        child: IndexedStack(
                          children: <Widget>[
                            Visibility(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    '${'\u202E' + course.description}',
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(height: 1.75),
                                  ),
                                ],
                              ),
                              maintainState: true,
                              visible: _tabController.index == 0,
                            ),
                            Visibility(
                              child: Column(
                                children: <Widget>[Comments(courseId: course.id)],
                              ),
                              maintainState: true,
                              visible: _tabController.index == 1,
                            ),
                          ],
                          index: _tabController.index,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}

class Comments extends StatefulWidget {
  final int courseId;
  Comments({Key key, this.courseId}) : super(key: key);

  @override
  _CommentsState createState() => _CommentsState();
}

class _CommentsState extends State<Comments> {
  GlobalKey<_CommentsListState> _keyCommentsList = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);
    final bool isAuthenticated = authProvider.status == Status.Authenticated;

    return Column(
      children: <Widget>[
        isAuthenticated != true
            ? Column(
                children: <Widget>[
                  Text('جهت ثبت امتیاز و نظر ابتدا باید وارد شوید:'),
                  SizedBox(
                    height: 15,
                  ),
                  RaisedButton.icon(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    elevation: 2,
                    icon: Icon(Icons.person),
                    label: Text('ورود'),
                    color: Colors.white,
                    textColor: primaryColor,
                    onPressed: () async {
                      final isLoggedIn = await Navigator.pushNamed(context, LoginScreen.routeName);

                      if (isLoggedIn) {
                        // Force to rebuild stateless widget
//                        (context as Element).markNeedsBuild();
                        _keyCommentsList.currentState._runFuture();
                      }
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              )
            : SizedBox(
                height: 0,
              ),
        CommentsList(
          key: _keyCommentsList,
          courseId: widget.courseId,
          isAuthenticated: isAuthenticated,
        ),
      ],
    );
  }
}

class RatingStarBar extends StatelessWidget {
  final int courseId;
  RatingStarBar({this.courseId});

  @override
  Widget build(BuildContext context) {
    int rating = 0;
    return StatefulBuilder(builder: (context, setState) {
      return StarRating(
        onChanged: (index) async {
//          setState(() {
//            rating = index;
//          });
          int result = await showDialog(
            context: context,
            builder: (_) => CommentDialog(courseId: courseId, rating: index),
          );
        },
        size: 30,
        value: rating,
      );
    });
  }
}

class CommentDialog extends StatefulWidget {
  final int courseId;
  final int rating;
  CommentDialog({Key key, this.courseId, this.rating}) : super(key: key);

  @override
  _CommentDialogState createState() => _CommentDialogState();
}

class _CommentDialogState extends State<CommentDialog> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String commentText;

  Future submit() async {
    final form = _formKey.currentState;

    if (form.validate()) {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      try {
        var response = await ApiService(authProvider)
            .submitComment(courseId: widget.courseId, rating: widget.rating, text: commentText);
        print(response);
      } catch (error) {
        throw Exception(error.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: Text('ثبت امتیاز و دیدگاه'),
      ),
      content: Form(
          key: _formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                enableSuggestions: false,
                keyboardType: TextInputType.text,
                style: TextStyle(fontSize: 14, color: Colors.black54),
                minLines: 2,
                maxLines: 5,
                textAlign: TextAlign.right,
                decoration: Styles.input.copyWith(
                  errorText: null,
                  counterText: "",
                  labelText: 'متن دیدگاه',
                  labelStyle: TextStyle(color: Colors.black),
                ),
                validator: (value) {
                  commentText = value.trim();

                  return Validate.requiredField(value, 'متن دیدگاه را وارد کنید');
                },
              ),
            ],
          )),
      actions: <Widget>[
        FlatButton(
          child: Text('انصراف'),
          onPressed: Navigator.of(context).pop,
        ),
        FlatButton(
          child: Text('ثبت'),
          onPressed: () async {
            FocusScope.of(context).unfocus(); // un-focus opened keyboard
            await submit();
          },
        )
      ],
    );
  }
}

class CommentsList extends StatefulWidget {
  final int courseId;
  final bool isAuthenticated;

  CommentsList({Key key, this.courseId, this.isAuthenticated}) : super(key: key);

  @override
  _CommentsListState createState() => _CommentsListState();
}

class _CommentsListState extends State<CommentsList> {
  Future _future;

  @override
  void initState() {
    super.initState();
    _future = _fetchComments();
  }

  Future _fetchComments() async {
    print('_fetchComments');
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response;
    try {
      response = await ApiService(authProvider).getComments(widget.courseId);
    } catch (error) {
      throw Exception(error.message);
    }

    return response;
  }

  _runFuture() {
    _future = _fetchComments();
  }

  @override
  Widget build(BuildContext context) {
    print('isAuthenticated: ${widget.isAuthenticated}');

    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text("${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
//          if (widget.refreshList) {
////            setState(() {
////              _listFuture = _fetchComments();
////            });
////          }

          List<Comment> comments = snapshot.data;

          return Column(
            children: <Widget>[
              comments[0].ownedByCurrentUser != true && widget.isAuthenticated
                  ? Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          'به این دوره امتیاز دهید:',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        RatingStarBar(
                          courseId: widget.courseId,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    )
                  : SizedBox(
                      height: 0,
                    ),
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: comments.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    Comment comment = comments[index];

                    return Container(
                      padding: EdgeInsets.all(10.0),
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                            color: comment.ownedByCurrentUser
                                ? primaryColor
                                : (Provider.of<ThemeProvider>(context).isLightTheme
                                    ? Colors.grey.shade100
                                    : Colors.grey.shade600),
                            width: 1),
                        color: Theme.of(context).appBarTheme.color,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          comment.ownedByCurrentUser == true
                              ? Column(
                                  children: <Widget>[
                                    Text('نظر شما:'),
                                    SizedBox(height: 10),
                                  ],
                                )
                              : SizedBox(
                                  height: 0,
                                ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              StarDisplayWidget(
                                value: double.parse(comment.stars),
                              ),
                              Text(
                                comment.createdAt,
                                textDirection: TextDirection.ltr,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(comment.content),
                        ],
                      ),
                    );
                  }),
            ],
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}

class CourseScreen extends StatelessWidget {
  static const routeName = '/course';

  Widget _getCoursePrice(String price) {
    final String formattedPrice =
        price.replaceAllMapped(new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]}٬');
    final String postfix = 'ریال';

    return Text(
      '$formattedPrice $postfix',
      style: TextStyle(fontSize: 18),
    );
  }

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);

    final CourseScreenArguments args = ModalRoute.of(context).settings.arguments;

    print('CourseScreen');

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: getAppBarActions(context, themeProvider, Colors.white),
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: CourseContent(args.index, args.id, args.category)),
      ),
    );
  }
}

class CourseScreenArguments {
  final int index;
  final int id;
  final CourseCategory category;

  CourseScreenArguments(this.index, this.id, this.category);
}
