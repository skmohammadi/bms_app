﻿import 'dart:async';

import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/theme/styles.dart';
import 'package:bmscience/utils/HexColor.dart';
import 'package:bmscience/utils/forms.dart';
import 'package:bmscience/utils/network_util.dart';
import 'package:bmscience/utils/validate.dart';
import 'package:bmscience/widgets/styled_flat_button.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatelessWidget {
  static const routeName = '/register';

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: WillPopScope(
        onWillPop: () async {
          if (authProvider.status == Status.Registering) {
            return false;
          }
          if (authProvider.status != Status.Unauthenticated) {
            authProvider.initAuthProvider();
          }

          return true;
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
//            backgroundColor: Colors.transparent,
            title: Text('ایجاد حساب کاربری'),
            leading: BackButton(
              onPressed: () {
                if (authProvider.status != Status.Unauthenticated ||
                    authProvider.notification != null) {
                  authProvider.initAuthProvider();
                }
                Navigator.of(context).pop();
              },
            ),
            actions: <Widget>[
              // Dark Mode Toggle Button
              Switch(
                inactiveThumbImage: AssetImage('assets/images/sun-black.png'),
                activeThumbImage: AssetImage('assets/images/moon-white.png'),
                activeColor: Colors.black,
                activeTrackColor: Colors.grey,
                value: !themeProvider.isLightTheme,
                onChanged: (value) {
                  themeProvider.setThemeData = !value;
                },
              ),
            ],
          ),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
//                  height: double.infinity,
                  decoration: BoxDecoration(
                    color: Theme.of(context).appBarTheme.color,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Theme.of(context).appBarTheme.color,
                      width: 1,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: RegisterForm(),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key key}) : super(key: key);

  @override
  RegisterFormState createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final FocusNode _mobileFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _universityEntranceFocus = FocusNode();

  String mobile;
  String email;
  String universityEntrance;
  String message = '';

  Map response = new Map();

  Future<void> submit() async {
    final form = _formKey.currentState;
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    if (form.validate()) {
      if (!await NetworkUtil().hasInternet()) {
        showNotification(context,
            message: 'اتصال دستگاه به اینترنت را بررسی کنید');
      } else {
//        response =
        await authProvider.register(mobile, email, universityEntrance);
        if (authProvider.notification != null) {
          showNotification(
            context,
            message: authProvider.notification,
            onDismiss: authProvider.status == Status.Registered
                ? () => Navigator.pop(context)
                : null,
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            /// Mobile
            TextFormField(
              focusNode: _mobileFocus,
//              initialValue: mobile,
              enableSuggestions: false,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.left,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _mobileFocus, _emailFocus);
              },
              style: TextStyle(letterSpacing: 6),
              maxLength: 11,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
              ],
              decoration: Styles.input.copyWith(
                errorText: null,
                counterText: "",
                labelText: 'شماره همراه',
                hintText: '09XXXXXXXXX',
                icon: Icon(Icons.phone_android),
              ),
              validator: (value) {
                mobile = value.trim();
                return Validate.validateMobile(value);
              },
            ),

            /// Email
            SizedBox(height: 15.0),
            TextFormField(
                focusNode: _emailFocus,
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (term) {
                  _fieldFocusChange(
                      context, _emailFocus, _universityEntranceFocus);
                },
                textAlign: TextAlign.left,
                keyboardType: TextInputType.emailAddress,
                decoration: Styles.input.copyWith(
                  labelText: 'پست الکترونیکی',
                  hintText: 'name@company.com',
                  icon: Icon(Icons.email),
                ),
                validator: (value) {
                  email = value.trim();
                  return Validate.validateEmail(value);
                }),

            /// UniversityEntrance
            SizedBox(height: 15.0),
            TextFormField(
                focusNode: _universityEntranceFocus,
                textAlign: TextAlign.left,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                ],
                decoration: Styles.input.copyWith(
                  labelText: 'ورودی سال',
                  icon: Icon(Icons.school),
                ),
                validator: (value) {
                  universityEntrance = value.trim();
                  return Validate.validateUniversityEntrance(value);
                }),
            SizedBox(height: 45.0),
            StyledFlatButton(
              'ایجاد حساب کاربری',
              onPressed: () {
                FocusScope.of(context).unfocus(); // un-focus opened keyboard
                submit();
              },
              isLoading:
                  Provider.of<AuthProvider>(context, listen: false).status ==
                      Status.Registering,
            ),
            SizedBox(height: 15.0),
            StyledFlatButton(
              'ورود به حساب',
              onPressed: () {
                Navigator.pop(context);
              },
              color: Theme.of(context).accentColor,
            ),
          ]..removeWhere((widget) => widget == null),
        ),
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
