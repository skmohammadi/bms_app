﻿import 'package:bmscience/models/chapter.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/views/Chapter.dart';
import 'package:bmscience/widgets/ListTileLeadingImage.dart';
import 'package:bmscience/widgets/getAppBarActions.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChaptersScreen extends StatelessWidget {
  static const routeName = '/chapters';

  @override
  Widget build(BuildContext context) {
    print('ChaptersScreen');

    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final ChaptersScreenArguments args = ModalRoute.of(context).settings.arguments;

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: getAppBarActions(context, themeProvider, Colors.white),
        ),
        body: ChaptersList(courseId: args.courseId, courseTitle: args.courseTitle),
      ),
    );
  }
}

class ChaptersList extends StatefulWidget {
  final int courseId;
  final String courseTitle;

  ChaptersList({Key key, this.courseId, this.courseTitle}) : super(key: key);

  @override
  _ChaptersListState createState() => _ChaptersListState();
}

class _ChaptersListState extends State<ChaptersList> {
  Future _future;

  @override
  void initState() {
    super.initState();
    _future = _fetchChapters();
  }

  Future _fetchChapters() async {
    print('_fetchChapters');

    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response;
    try {
      response = await ApiService(authProvider).getChapters(widget.courseId);
    } catch (error) {
      throw Exception(error.message);
    }

    return response;
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final bool isLoggedIn = authProvider.status == Status.Authenticated;

    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          print(snapshot);
          return Center(
            child: Text("${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          List<Chapter> chapters = snapshot.data;
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListView.builder(
                itemCount: chapters.length,
                itemBuilder: (context, index) {
                  Chapter chapter = chapters[index];

                  return Card(
                    color: Colors.white.withOpacity(.2),
                    elevation: 0,
                    child: ListTile(
                      onTap: () async {
                        if (isLoggedIn) {
                          Navigator.pushNamed(
                            context,
                            ChapterScreen.routeName,
                            arguments: ChapterScreenArguments(
                              chapter.id,
                              chapter.title,
                              chapter.icon,
                              chapter.favoritesCount,
                              widget.courseTitle,
                            ),
                          );
                        } else {
                          await showLoginNeededDialog(context);
                        }
                      },
                      title: Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Text(chapter.title),
                      ),
                      subtitle: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.cloud_download,
                            color: primaryColor,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text('${chapter.downloadsCount}')
                        ],
                      ),
                      leading: ListTileLeadingImage(chapter.icon),
                      trailing: isLoggedIn
                          ? const Icon(Icons.chevron_right)
                          : const Icon(Icons.lock_outline),
                    ),
                  );
                }),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}

class ChaptersScreenArguments {
  final int courseId;
  final String courseTitle;

  ChaptersScreenArguments(this.courseId, this.courseTitle);
}
