﻿import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/course.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/utils/app_icons.dart';
import 'package:bmscience/widgets/courses_list.dart';
import 'package:bmscience/widgets/HomeTabBar.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('خروج از برنامه'),
            content: new Text('آیا برای خروج از برنامه اطمینان دارید؟'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("خیر"),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text("بله"),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    print('HomeScreen');

    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
//            title: Text('برنامه'),
            elevation: 6,
            leading: IconButton(
              icon: Icon(
                authProvider.status == Status.Authenticated
                    ? BMSIcons.user_md
                    : Icons.person,
                color: Theme.of(context).primaryIconTheme.color,
              ),
              onPressed: () {
                if (authProvider.status == Status.Authenticated) {
                  Navigator.pushNamed(context, '/profile');
                }
              },
            ),
            actions: [
              // App Share Button
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("اشتراک گذاری"),
                        content: Text("متن خالی"),
                      );
                    },
                  );
                },
              ),

              // Dark Mode Toggle Button
              Switch(
                inactiveThumbImage: AssetImage('assets/images/sun-black.png'),
                activeThumbImage: AssetImage('assets/images/moon-white.png'),
                activeColor: Colors.black,
                activeTrackColor: Colors.grey,
                value: !themeProvider.isLightTheme,
                onChanged: (value) {
                  themeProvider.setThemeData = !value;
                },
              ),
            ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(80),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: HomeTabBar(_tabController, themeProvider.isLightTheme),
              ),
            ),
          ),
          body: CourseTabs(_tabController),
        ),
      ),
    );
  }
}

class CourseTabs extends StatefulWidget {
  final TabController tabController;

  CourseTabs(this.tabController, {Key key}) : super(key: key);

  @override
  _CourseTabsState createState() => _CourseTabsState();
}

class _CourseTabsState extends State<CourseTabs> {
  Future _getCourses;

  @override
  void initState() {
    super.initState();
    _getCourses = getCourses();
  }

  Future getCourses() async {
    print('getCourses');
    final courseProvider = Provider.of<CourseProvider>(context, listen: false);

    if (courseProvider.initialized == false) {
      try {
        await courseProvider.init();
      } catch (error) {
        throw Exception(error.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final courseProvider = Provider.of<CourseProvider>(context, listen: false);
    /* TODO: Prevent redundant build on changing the route */
    return FutureBuilder(
      future: _getCourses,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          print(snapshot);
          return Center(
            child: Text(
                "${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: TabBarView(
              controller: widget.tabController,
              children: [
                CoursesList(
                    courseProvider.medicalCourses, CourseCategory.medical),
                CoursesList(
                    courseProvider.dentistryCourses, CourseCategory.dentistry),
              ],
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}
