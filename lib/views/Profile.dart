﻿import 'package:bmscience/models/User.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/widgets/styled_flat_button.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatelessWidget {
  static const routeName = '/profile';

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    return Container(
      decoration: getScreenBackground(themeProvider.isLightTheme),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text('پروفایل'),
          leading: BackButton(
            onPressed: () {
              if (authProvider.status != Status.Unauthenticated ||
                  authProvider.notification != null) {
                authProvider.initAuthProvider();
              }
              Navigator.of(context).pop();
            },
          ),
          actions: <Widget>[
            // Dark Mode Toggle Button
            Switch(
              inactiveThumbImage: AssetImage('assets/images/sun-black.png'),
              activeThumbImage: AssetImage('assets/images/moon-white.png'),
              activeColor: Colors.black,
              activeTrackColor: Colors.grey,
              value: !themeProvider.isLightTheme,
              onChanged: (value) {
                themeProvider.setThemeData = !value;
              },
            ),
          ],
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).appBarTheme.color,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: Theme.of(context).appBarTheme.color,
                  width: 1,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: ProfileContent(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileContent extends StatefulWidget {
  @override
  _ProfileContentState createState() => _ProfileContentState();
}

class _ProfileContentState extends State<ProfileContent> {
  bool loggingOut = false;
  Future _future;

  Future _fetchUserProfile() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response;
    try {
      response = await ApiService(authProvider).getUser();
      print(response);
    }
//    on AuthException catch (exception) {
//      throw Exception(exception.message);
//    }
    catch (error) {
      throw Exception(error.message);
    }

    return response;
  }

  @override
  void initState() {
    super.initState();
    _future = _fetchUserProfile();
  }

  Future<void> logout() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    setState(() {
      loggingOut = true;
    });
    await authProvider.logOut();
    if (authProvider.status == Status.Unauthenticated) {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(
                "${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          User user = snapshot.data;
          return Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: CircleAvatar(
                  radius: 50,
                  backgroundImage:
                      AssetImage('assets/images/nurse-avatar-m.png'),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('نام و نام خانوادگی:'),
                  Text(user.name),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('ایمیل:'),
                  Text(user.email),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('کد ملی:'),
                  Text(user.melliCode),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('ورود به دانشگاه:'),
                  Text('${user.universityEntrance}'),
                ],
              ),
              SizedBox(
                height: 45.0,
              ),
              StyledFlatButton(
                'خروج از حساب',
                onPressed: logout,
                isLoading: loggingOut,
              )
            ],
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}
