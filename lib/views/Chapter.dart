﻿import 'package:bmscience/models/part.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/views/Quiz.dart';
import 'package:bmscience/widgets/ListTileLeadingImage.dart';
import 'package:bmscience/widgets/getAppBarActions.dart';
import 'package:bmscience/widgets/styled_flat_button.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChapterScreenArguments {
  final int chapterId;
  final String chapterTitle;
  final String chapterIcon;
  final int favoritesCount;
  final String courseTitle;

  ChapterScreenArguments(
      this.chapterId, this.chapterTitle, this.chapterIcon, this.favoritesCount, this.courseTitle);
}

class ChapterScreen extends StatefulWidget {
  static const routeName = '/chapter';

  @override
  _ChapterScreenState createState() => _ChapterScreenState();
}

class _ChapterScreenState extends State<ChapterScreen> {
  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final ChapterScreenArguments args = ModalRoute.of(context).settings.arguments;

    GlobalKey<ChapterFABState> _fabKey = GlobalKey();

    return ChapterProvider(
      child: Container(
        decoration: getScreenBackground(themeProvider.isLightTheme),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: primaryColor,
            leading: IconButton(
              color: Colors.white,
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: getAppBarActions(context, themeProvider, Colors.white),
          ),
          body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: ChapterContent(
                args.chapterId,
                args.chapterTitle,
                args.chapterIcon,
                args.courseTitle,
                args.favoritesCount,
              )),
//            floatingActionButtonLocation: chapterFABLocation,
          floatingActionButton: ChapterFAB(key: _fabKey),
        ),
      ),
      fabKey: _fabKey,
    );
  }
}

class ChapterFAB extends StatefulWidget {
  ChapterFAB({Key key}) : super(key: key);

  @override
  ChapterFABState createState() => ChapterFABState();
}

class ChapterFABState extends State<ChapterFAB> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation _fabAnimation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 250));
    final Animation curve = CurvedAnimation(parent: controller, curve: Curves.easeOut);
    _fabAnimation = Tween<double>(begin: 80.0, end: 0.0).animate(curve);
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future showPackageTypeDialog(BuildContext context, List<int> selectedParts) async {
    final String title = 'آزمون‌ساز';

    void go(int packageSize) async {
      Navigator.pop(context, true);

      var result = await Navigator.pushNamed(
        context,
        QuizScreen.routeName,
        arguments: QuizScreenArguments(selectedParts, packageSize),
      );

      /// TODO: check number of bookmarked questions that returned form QuizScreen
      if (result == '12') {
        ChapterProvider.of(context).clearSelectedItems();
      }
    }

    AlertDialog alert = AlertDialog(
      title: Center(
        child: Text(title),
      ),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: 'تعداد بخش‌های انتخابی: ',
                  style: TextStyle(
                      fontFamily: 'Shabnam FD', color: Colors.grey.shade600, fontSize: 14),
                  children: [
                    TextSpan(
                      text: '${selectedParts.length}',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ]),
            ),
//            SizedBox(height: 20),
//            Text('یکی از بسته‌ها را انتخاب کنید:'),
            SizedBox(height: 20.0),
            StyledFlatButton(
              'بسته 20 سوالی',
              onPressed: () => go(20),
              color: primaryColor,
            ),
            SizedBox(height: 5),
            StyledFlatButton(
              'بسته 40 سوالی',
              onPressed: () => go(40),
              color: accentColor,
            ),
            SizedBox(height: 5),
            StyledFlatButton(
              'بسته 60 سوالی',
              onPressed: () => go(60),
              color: primaryColor,
            ),
          ],
        ),
      ),
    );

    await showDialog(
      context: context,
      builder: (_) => alert,
    );
  }

  @override
  Widget build(BuildContext context) {
    final container = ChapterProvider.of(context);
    return _fabAnimation.value < 80
        ? Transform.translate(
            offset: Offset(0, _fabAnimation.value),
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                showPackageTypeDialog(context, container.selectedItems);
//
              },
              label: Text('شروع آزمون'),
              icon: Icon(Icons.timer),
              backgroundColor: accentColor,
            ),
          )
        : SizedBox();
  }
}

class ChapterContent extends StatelessWidget {
  final int chapterId;
  final String chapterTitle;
  final String chapterIcon;
  final String courseTitle;
  final int favoritesCount;

  ChapterContent(
    this.chapterId,
    this.chapterTitle,
    this.chapterIcon,
    this.courseTitle,
    this.favoritesCount,
  );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          ChapterHeader(chapterTitle, chapterIcon, courseTitle),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                favoritesCount > 0 ? ChapterBookmarks() : Container(),
                SizedBox(height: 20.0),
                ChapterParts(
                  chapterId: chapterId,
                ),
              ],
            ),
          ),
//        ChapterParts()
        ],
      ),
    );
  }
}

class ChapterHeader extends StatelessWidget {
  ChapterHeader(
    this.chapterTitle,
    this.chapterIcon,
    this.courseTitle,
  );

  final String chapterTitle;
  final String chapterIcon;
  final String courseTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColorLight,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 12.0),
        child: ListTile(
          contentPadding: EdgeInsets.all(0),
          title: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              'دوره: $courseTitle',
              style: TextStyle(fontSize: 14.0, color: Colors.white),
            ),
          ),
          subtitle: Text(
            'فصل: $chapterTitle',
            style: TextStyle(fontSize: 24.0, color: Colors.white),
          ),
          leading: ListTileLeadingImage(chapterIcon),
        ),
      ),
    );
  }
}

class ChapterBookmarks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: primaryColor,
      child: ListTile(
        title: Row(
          children: <Widget>[
            Text(
              'سوالات ذخیره‌شده',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: 40,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50.0),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
                child: Center(child: Text('12')),
              ),
            )
          ],
        ),
        leading: Icon(
          Icons.bookmark,
          color: Colors.white,
        ),
        trailing: const Icon(
          Icons.chevron_right,
          color: Colors.white,
        ),
      ),
    );
  }
}

class ChapterParts extends StatefulWidget {
  final int chapterId;
  ChapterParts({Key key, this.chapterId}) : super(key: key);

  @override
  _ChapterPartsState createState() => _ChapterPartsState();
}

class _ChapterPartsState extends State<ChapterParts> {
  Future _future;

  Map<int, GlobalKey<_PartItemState>> _selectedItems = new Map();

  @override
  void initState() {
    super.initState();
    _future = _fetchChapterParts();
  }

  Future _fetchChapterParts() async {
    print('fetchParts');

    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    var response;
    try {
      response = await ApiService(authProvider).getParts(widget.chapterId);
    } catch (error) {
      throw Exception(error.message);
    }

    return response;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text("${snapshot.error.toString().replaceAll("Exception", "خطا")}"),
          );
        } else if (snapshot.connectionState == ConnectionState.done) {
          List<Part> parts = snapshot.data;

          return ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: parts.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                Part part = parts[index];
                var key = new GlobalKey<_PartItemState>();
                _selectedItems.putIfAbsent(part.id, () => key);

                return part.questionsCount > 0 ? PartItem(part, key: key) : null;
              });
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
            ),
          );
        }
      },
    );
  }
}

class PartItem extends StatefulWidget {
  final Part part;

  PartItem(this.part, {Key key}) : super(key: key);

  @override
  _PartItemState createState() => _PartItemState();
}

class _PartItemState extends State<PartItem> {
  bool isSelected = false;
  bool isPurchased = false;

  @override
  void initState() {
    super.initState();
    print('initState');
  }

  Future purchasePart() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);

    try {
      final result = await ApiService(authProvider).purchasePart(widget.part.id);
      print('result: $result');
      if (result) {
        widget.part.setPurchased();
        setState(() {
          isPurchased = true;
        });
      }
    } catch (error) {
      throw Exception(error.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    final container = ChapterProvider.of(context);

    isPurchased = widget.part.isPurchased();
    return Card(
      color: Colors.white.withOpacity(.2),
      elevation: 0,
      child: ListTile(
        selected: isSelected,
        onTap: () async {
          if (isPurchased) {
            _toggleSelection();
            container.toggleSelection(widget.part.id);
          } else {
            // TODO: redirect user to checkout
//            await purchasePart();
          }
        },
        title: Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text('${widget.part.title} (${widget.part.questionsCount})'),
        ),
        subtitle: Row(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.monetization_on,
                  size: 18,
                  color: primaryColorLight,
                ),
                SizedBox(width: 5.0),
                Text('${isPurchased ? "خریداری‌شده" : widget.part.price}'),
              ],
            ),
            SizedBox(width: 15.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_download,
                  size: 18,
                  color: primaryColorLight,
                ),
                SizedBox(width: 5.0),
                Text('${widget.part.downloadsCount} دانلود'),
              ],
            ),
          ],
        ),
        leading: isPurchased
            ? Icon(
                Icons.lock_open,
                color: Colors.green,
              )
            : Icon(Icons.lock_outline),
        trailing: isSelected
            ? Icon(Icons.check_box)
            : !isPurchased
                ? Icon(Icons.shopping_basket)
                : Icon(
                    Icons.check_box_outline_blank,
                  ),
      ),
    );
  }

  void _toggleSelection() {
    setState(() {
      if (isSelected) {
        isSelected = false;
      } else {
        isSelected = true;
      }
    });
  }
}

/// Chapter State Container
class ChapterProvider extends StatefulWidget {
  final Widget child;
  final GlobalKey<ChapterFABState> fabKey;

  const ChapterProvider({Key key, @required this.child, this.fabKey}) : super(key: key);

  static _ChapterProviderState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedChapterProvider>().data;
  }

  @override
  _ChapterProviderState createState() => _ChapterProviderState();
}

class _ChapterProviderState extends State<ChapterProvider> {
  List<int> selectedItems = [];

  void toggleSelection(int partId) {
    print('toggleSelection');
    setState(() {
      if (selectedItems.contains(partId)) {
        selectedItems.remove(partId);
      } else {
        selectedItems.add(partId);
      }
    });

    if (selectedItems.isNotEmpty) {
      widget.fabKey.currentState.controller.forward();
    } else {
      widget.fabKey.currentState.controller.reverse();
    }
  }

  void clearSelectedItems() {
    setState(() {
      selectedItems = [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedChapterProvider(
      child: widget.child,
      data: this,
    );
  }
}

class _InheritedChapterProvider extends InheritedWidget {
  final _ChapterProviderState data;

  _InheritedChapterProvider({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedChapterProvider old) => true;
}
