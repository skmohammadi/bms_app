﻿import 'dart:async';
import 'package:bmscience/models/quiz.dart';
import 'package:bmscience/providers/auth.dart';
import 'package:bmscience/providers/theme.dart';
import 'package:bmscience/services/api.dart';
import 'package:bmscience/theme/themes.dart';
import 'package:bmscience/widgets/CircularProgress.dart';
import 'package:bmscience/widgets/getAppBarActions.dart';
import 'package:bmscience/widgets/styled_flat_button.dart';
import 'package:bmscience/widgets/view_bg_decoration.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class QuizContent extends StatefulWidget {
  QuizContent({Key key}) : super(key: key);

  @override
  _QuizContentState createState() => _QuizContentState();
}

class _QuizContentState extends State<QuizContent> {
  @override
  Widget build(BuildContext context) {
    final quizProvider = QuizProvider.of(context);

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        quizProvider.status == QuizStatus.Initialized
            ? QuizHeader(
                questionsCount: quizProvider.quiz.getQuestionsCount,
                questionIndex: quizProvider._currentQuestionNumber(),
              )
            : (quizProvider.status == QuizStatus.Initializing)
                ? Expanded(
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
                      ),
                    ),
                  )
                : Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('آماده‌سازی آزمون ناموفق بود!'),
                        SizedBox(height: 10),
                        FlatButton.icon(
                          label: Text('تلاش دوباره'),
                          color: primaryColorLight,
                          icon: Icon(Icons.refresh),
                          onPressed: () => quizProvider._buildQuiz(),
                        )
                      ],
                    ),
                  ),
        quizProvider.status == QuizStatus.Initialized ? LinearProgressBar() : Container(),
        quizProvider.status == QuizStatus.Initialized ? Expanded(child: QuizBody()) : Container(),
      ],
    );
  }
}

class LinearProgressBar extends StatefulWidget {
  @override
  _LinearProgressBarState createState() => _LinearProgressBarState();
}

class _LinearProgressBarState extends State<LinearProgressBar> {
  Color _getItemFillColor(int index) {
    final quizProvider = QuizProvider.of(context);

    Color _color;

    if (index >= quizProvider.answersResult.length) {
      _color = Colors.white70;

      return _color;
    }

    if (index + 1 == quizProvider._currentQuestionNumber()) {
      _color = primaryColor;
    }

    // answered previously
    bool result = quizProvider.answersResult[index] == 1;
    _color = result == true ? Colors.green : Colors.red;

    return _color;
  }

  @override
  Widget build(BuildContext context) {
    final quizProvider = QuizProvider.of(context);
    return Container(
      height: 20,
      color: Colors.white70,
      child: Center(
        child: ListView.builder(
          controller: quizProvider._progressController,
          itemCount: quizProvider.quiz.getQuestionsCount,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext ctx, int index) {
            Color itemFillColor = _getItemFillColor(index);
            Color textColor = itemFillColor == Colors.white70 ? primaryTextColor : Colors.white70;

            return Container(
              height: 20,
              decoration: BoxDecoration(
                  color: itemFillColor,
                  border: Border(
                    bottom: BorderSide(color: Colors.white70, width: 1.0),
                    left: BorderSide(color: Colors.white70, width: 1),
                  )),
              width: MediaQuery.of(context).size.width / 10,
              child: Center(
                child: Text(
                  '${index + 1}',
                  style: TextStyle(color: textColor),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class QuizBody extends StatefulWidget {
  @override
  _QuizBodyState createState() => _QuizBodyState();
}

class _QuizBodyState extends State<QuizBody> {
  _QuizProviderState _quizProvider;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _quizProvider = QuizProvider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: PageView.builder(
          controller: _quizProvider._pageController,
          physics: NeverScrollableScrollPhysics(),
          itemCount: _quizProvider.quiz.getQuestionsCount,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white54,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Theme.of(context).appBarTheme.color,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            /// Question Title
                            _buildQuestionTitle(),

                            SizedBox(height: 10),

                            /// Question Image
                            _buildQuestionImage(),

                            SizedBox(height: 30),

                            /// Question Options
                            _buildOptions()
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  Widget _buildQuestionTitle() {
    return Text(
      _quizProvider.quiz.getQuestionText(),
      textAlign: TextAlign.right,
      style: TextStyle(
        fontSize: _quizProvider.textSize,
      ),
    );
  }

  Widget _buildQuestionImage() {
    final imageUrl = _quizProvider.quiz.getQuestionImage();

    return imageUrl != null
        ? SizedBox(
            height: 70.0,
            child: CachedNetworkImage(
              imageUrl:
                  'https://images.freeimages.com/images/small-previews/05e/on-the-road-6-1384796.jpg',
              placeholder: (context, url) => Padding(
                padding: const EdgeInsets.all(20.0),
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
                ),
              ),
              errorWidget: (context, url, error) {
                print(error);
                return const Image(image: AssetImage('assets/images/course.png'));
              },
            ),
          )
        : Container();
  }

  Widget _buildOptions() {
    final List<String> options = _quizProvider.quiz.getQuestionOptions();

    return ListView.builder(
        itemCount: 4,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          final String option = options[index];
          final isSelected = index + 1 == _quizProvider.selectedOption;
          final isCorrect = index + 1 == _quizProvider.correctOption;
          Color selectedRadioColor = _quizProvider.selectedOptionColor;

          return ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 0),
            title: Text(option, style: TextStyle(fontSize: _quizProvider.textSize)),
            leading: isSelected
                ? Icon(
                    Icons.radio_button_checked,
                    color: selectedRadioColor,
                  )
                : Icon(
                    isCorrect ? Icons.radio_button_checked : Icons.radio_button_unchecked,
                    color: isCorrect ? Colors.green : Colors.grey,
                  ),
            onTap: () {
              _quizProvider.setSelectedOption(index + 1);
            },
          );
        });
  }
}

class QuizHeader extends StatefulWidget {
  final int questionsCount;
  final int questionIndex;

  QuizHeader({Key key, @required this.questionsCount, @required this.questionIndex})
      : super(key: key);

  @override
  _QuizHeaderState createState() => _QuizHeaderState();
}

class _QuizHeaderState extends State<QuizHeader> {
  @override
  Widget build(BuildContext context) {
    final quizProvider = QuizProvider.of(context);

    return Container(
      color: primaryColorLight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    text: 'سوال ${widget.questionIndex} ',
                    style: TextStyle(
                        fontFamily: 'Shabnam FD', fontWeight: FontWeight.bold, fontSize: 18.0),
                    children: [
                      TextSpan(
                        text: '/ ${widget.questionsCount}',
                        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0),
                      ),
                      WidgetSpan(
                          child: GestureDetector(
                        child: Icon(
                          Icons.bookmark,
                          color: quizProvider.bookmarked ? Colors.red : Colors.white,
                        ),
                        onTap: () =>
                            quizProvider.toggleBookmark(quizProvider.quiz.currentQuestionId),
                      )),
                      WidgetSpan(
                          child: Container(
                        width: 10.0,
                      )),
                    ],
                  ),
                ),

                /// Timer
                CountDown(key: quizProvider._countDownKey),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CountDown extends StatefulWidget {
  CountDown({Key key}) : super(key: key);

  @override
  _CountDownState createState() => _CountDownState();
}

class _CountDownState extends State<CountDown> with SingleTickerProviderStateMixin {
  AnimationController progressController;
  Animation<double> animation;

  final maxTime = 10;
  int elapsedSeconds;

  TextStyle secondsStyle = TextStyle(
    fontSize: 16.0,
    letterSpacing: 1,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );

  Color backgroundColor = primaryColor;
  Color foregroundColor = accentColor;

  @override
  void initState() {
    super.initState();
    progressController = AnimationController(vsync: this, duration: Duration(seconds: maxTime));
    progressController.forward();
    animation = Tween<double>(begin: 0, end: 100).animate(progressController)
      ..addListener(() {
        setState(() {
          elapsedSeconds = percent2second(animation.value);
        });
      });
  }

  @override
  void dispose() {
    progressController.dispose();
    super.dispose();
  }

  void _resetProgress() {
    progressController.reset();
    progressController.forward();
  }

  int _stopProgress() {
    progressController.stop();
    print(maxTime - elapsedSeconds);
    return maxTime - elapsedSeconds;
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      foregroundPainter: CircularProgress(
        animation.value,
        strokeWidth: 5,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
      ),
      child: Container(
        width: 50,
        height: 50,
        child: Center(
          child: Text(
            '$elapsedSeconds',
            style: secondsStyle,
          ),
        ),
      ),
    );
  }

  int percent2second(double percent) {
    /// f(p) = c + (d - c)/(b - a) * (p - a)
    return (0 + (maxTime / 100) * percent).toInt();
  }
}

class QuizScreen extends StatelessWidget {
  static const routeName = '/quiz';

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    final QuizScreenArguments args = ModalRoute.of(context).settings.arguments;
    final _appBarKey = GlobalKey<State<AppBar>>();

    List<Widget> appBarActions = getAppBarActions(context, themeProvider, Colors.white);
    appBarActions.removeAt(0);
    appBarActions.insert(
      0,
      IconButton(
        color: Colors.white,
        icon: Icon(Icons.bug_report),
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("اشتراک گذاری"),
                content: Text("متن خالی"),
              );
            },
          );
        },
      ),
    );

    return QuizProvider(
      child: Container(
        decoration: getScreenBackground(themeProvider.isLightTheme),
        child: WillPopScope(
          onWillPop: () async {
            _onBackPressed(context).then((value) {
              if (value != false) {
                Navigator.pop(context, value);
              }
            });
//            Navigator.pop(context, '13');
            return false;
          },
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              key: _appBarKey,
              backgroundColor: primaryColor,
              leading: TextResizePopup(parentKey: _appBarKey),
              actions: appBarActions,
            ),
            body: QuizContent(),
            bottomNavigationBar: QuizBottomNavigationBar(),
          ),
        ),
      ),
    );
  }

  Future _onBackPressed(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('خروج از آزمون‌ساز'),
        content: new Text('آیا برای خروج از آزمون‌ساز اطمینان دارید؟'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.pop(context, false),
            child: Text("انصراف"),
          ),
          FlatButton(
            /// TODO: return number of new bookmarked questions
            onPressed: () => Navigator.pop(context, '12'),
            child: Text("خروچ"),
          ),
        ],
      ),
    );
  }
}

class TextResizePopup extends StatefulWidget {
  final GlobalKey parentKey;

  TextResizePopup({this.parentKey});

  @override
  _TextResizePopupState createState() => _TextResizePopupState();
}

class _TextResizePopupState extends State<TextResizePopup> {
  var menuPosition;
  Map<String, double> textSize = {
    "Small": 12.0,
    "Normal": 14.0,
    "Large": 16.0,
  };

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 0), () {
      menuPosition = buttonMenuPosition(widget.parentKey.currentContext);
    });
  }

  RelativeRect buttonMenuPosition(BuildContext c) {
    final RenderBox bar = c.findRenderObject();
    final RenderBox overlay = Overlay.of(c).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    return position;
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      color: Colors.white,
      icon: Icon(Icons.format_size),
      onPressed: () async {
        final textSizeSelected = await showMenu(
          context: context,
          position: menuPosition,
          items: <PopupMenuItem<double>>[
            PopupMenuItem(
              height: 24.0,
              value: textSize['Small'],
              child: Text(
                "قلم ریز",
                style: TextStyle(fontSize: textSize['Small']),
              ),
            ),
            PopupMenuItem(
              height: 28.0,
              value: textSize['Normal'],
              child: Text(
                "قلم عادی",
                style: TextStyle(fontSize: textSize['Normal']),
              ),
            ),
            PopupMenuItem(
              height: 30.0,
              value: textSize['Large'],
              child: Text(
                "قلم درشت",
                style: TextStyle(fontSize: textSize['Large']),
              ),
            ),
          ],
        );
        if (textSizeSelected != null) {
          QuizProvider.of(context).textSizeSelected(textSizeSelected);
        }
      },
    );
  }
}

class QuizResultDialog extends StatelessWidget {
  final int totalAnswered;
  final int savedTime;

  QuizResultDialog(this.totalAnswered, this.savedTime);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: Text('نتیجه آزمون'),
      ),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 0),
              dense: true,
              leading: Icon(
                Icons.radio_button_checked,
                color: Colors.green,
              ),
              title: RichText(
                  text: TextSpan(
                text: 'پاسخ درست:',
                style: TextStyle(color: primaryTextColor, fontFamily: 'Shabnam FD'),
              )),
              trailing: Text(
                '$totalAnswered',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              dense: true,
              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 0),
              leading: Icon(
                Icons.timer,
                color: Colors.green,
              ),
              title: RichText(
                  text: TextSpan(
                text: 'زمان ذخیره‌شده:',
                style: TextStyle(color: primaryTextColor, fontFamily: 'Shabnam FD'),
              )),
              trailing: Text(
                '$savedTime',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class QuizBottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final quizProvider = QuizProvider.of(context);

    return quizProvider.status == QuizStatus.Initialized
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
            child: StyledFlatButton(
              quizProvider.buttonLabel,
              disabled: quizProvider.selectedOption == -1,
              onPressed: () async {
                if (quizProvider.quizFinished) {
                  await showDialog(
                    context: context,
                    builder: (_) =>
                        QuizResultDialog(quizProvider.totalAnswered, quizProvider.savedTime),
                  );
                } else {
                  quizProvider.checkAnswer();
                }
              },
            ),
          )
        : SizedBox();
  }
}

class QuizScreenArguments {
  final List<int> selectedParts;
  final int packageSize;

  QuizScreenArguments(this.selectedParts, this.packageSize);
}

/// Quiz State Container

enum QuizStatus {
  Initialized,
  Initializing,
  Failed,
}

class QuizProvider extends StatefulWidget {
  final Widget child;
  final List<int> selectedParts;
  final int packageSize;

  const QuizProvider({Key key, @required this.child, this.selectedParts, this.packageSize})
      : super(key: key);

  static _QuizProviderState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedQuizProvider>().data;
  }

  @override
  _QuizProviderState createState() => _QuizProviderState();
}

class _QuizProviderState extends State<QuizProvider> {
  /// Text Size State
  double textSize = 14.0;

  String buttonLabel = 'ثبت پاسخ';

  QuizStatus status = QuizStatus.Initializing;
  Quiz quiz;
  int selectedOption = -1;
  Color selectedOptionColor = accentColor;
  int correctOption = -1;
  int totalAnswered = 0;
  bool quizFinished = false;
  List<int> answersResult = [];
  bool bookmarked;

  ScrollController _progressController;
  PageController _pageController;

  GlobalKey<_CountDownState> _countDownKey;
  int savedTime = 0;

  @override
  void initState() {
    super.initState();
    _countDownKey = GlobalKey();
    _progressController = ScrollController();
    _pageController = PageController(
      initialPage: 0,
      viewportFraction: 1.0,
    );

    Future.delayed(Duration(seconds: 0), () {
      _buildQuiz(init: true);
    });
  }

  @override
  void dispose() {
    _progressController.dispose();
    super.dispose();
  }

  void _buildQuiz({bool init = false}) {
    print('_buildQuiz');

    if (!init) {
      setState(() {
        status = QuizStatus.Initializing;
      });
    }

    _fetchQuestions().then((questions) {
      setState(() {
        // TODO
        quiz = Quiz(questions);
//        quiz = Quiz(questions.sublist(0, 3));
        status = QuizStatus.Initialized;
        bookmarked = quiz.isQuestionBookmarked();
      });
    }).catchError((error) {
      setState(() {
        status = QuizStatus.Failed;
      });
    });
  }

  Future _fetchQuestions() async {
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final QuizScreenArguments args = ModalRoute.of(context).settings.arguments;

    var response;
    try {
      response = await ApiService(authProvider).getQuestions(args.selectedParts, args.packageSize);
    } catch (error) {
      throw Exception(error.message);
    }

    return response;
  }

  Future toggleBookmark(int questionId) async {
    setState(() {
      if (quiz.isQuestionBookmarked()) {
        /// TODO: need un-favorite api method
        bookmarked = false;
      } else {
        bookmarked = true;
        quiz.setQuestionBookmarked();

        final authProvider = Provider.of<AuthProvider>(context, listen: false);

        try {
          ApiService(authProvider).markAsFavoriteQuestion(questionId);
        } catch (error) {
          throw Exception(error.message);
        }
      }
    });
  }

  void setSelectedOption(int index) {
    setState(() {
      selectedOption = index;
    });
  }

  void checkAnswer() {
    setState(() {
      correctOption = quiz.getCorrectAnswer();
      if (selectedOption == correctOption) {
        selectedOptionColor = Colors.green;
        answersResult.add(1);
        totalAnswered += 1;
//          scoreKeeper.add(Icon(
//            Icons.check,
//            color: Colors.green,
//          ));
      } else {
        selectedOptionColor = Colors.red;
        answersResult.add(0);
//          scoreKeeper.add(Icon(
//            Icons.close,
//            color: Colors.red,
//          ));
      }

      savedTime += _countDownKey.currentState._stopProgress();
    });
    if (quiz.isFinished()) {
      print('finished');
      setState(() {
        quizFinished = true;
        buttonLabel = 'مشاهده نتیجه';
      });
    } else
      _nextQuestion();
  }

  int _currentQuestionNumber() {
    return quiz.currentQuestionIndex + 1;
  }

  void _nextQuestion() async {
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        correctOption = -1;
        selectedOption = -1;
        selectedOptionColor = accentColor;
        quiz.nextQuestion();
        bookmarked = quiz.isQuestionBookmarked();
      });
      _pageController.nextPage(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInOutQuart,
      );

      _progressBarAnimate(quiz.currentQuestionIndex);

      /// Reset countdown
      _countDownKey.currentState._resetProgress();
    });
  }

  void _progressBarAnimate(int index) {
    print('_progressBarAnimate: $index');
    print('${MediaQuery.of(context).size.width}');
    final offset = index * MediaQuery.of(context).size.width / 10;
    print(offset);
    _progressController.animateTo(offset, duration: new Duration(seconds: 1), curve: Curves.ease);
  }

  void textSizeSelected(double size) {
    setState(() {
      textSize = size;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedQuizProvider(
      child: widget.child,
      data: this,
    );
  }
}

class _InheritedQuizProvider extends InheritedWidget {
  final _QuizProviderState data;

  _InheritedQuizProvider({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedQuizProvider old) => true;
}
